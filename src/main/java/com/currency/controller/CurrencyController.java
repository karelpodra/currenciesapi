package com.currency.controller;

import com.currency.model.Currency;
import com.currency.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.CrossOrigin;


import java.util.List;


@RestController
@RequestMapping("")
@CrossOrigin("*")
public class CurrencyController {

    @Autowired
    private CurrencyRepository currencyRepository;

    @GetMapping("/currencies")
    public List<Currency> getAllCurrencies() {
        return currencyRepository.fetchCurrencies();
    }

    @PostMapping("/currencies")
    public void addCurrency(@RequestBody Currency currency) {
        currencyRepository.addCurrency(currency);
    }

    @PutMapping("/currencies/{id}")
    public void updateCurrency(@RequestBody Currency currency) {
        currencyRepository.updateCurrency(currency);
    }


    @DeleteMapping("/currencies/{id}")
    public void deleteSingleCurrency(@PathVariable Long id) {
        currencyRepository.deleteSingleCurrency(id);
    }






}
