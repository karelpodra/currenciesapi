package com.currency.repository;


import com.currency.model.Currency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

import static java.lang.Integer.parseInt;

@Repository
public class CurrencyRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public List<Currency> fetchCurrencies() {
        return jdbcTemplate.query("select * from currency",
                (row, number) -> {
                    return new Currency(
                            row.getInt("id"),
                            row.getString("name"),
                            row.getFloat("rate")
                    );
                });
    }



    public void updateCurrency(Currency currency) {
        jdbcTemplate.update("update currency set `name` = ?, `rate` = ? where `id` = ?",
                currency.getName(), currency.getRate(), currency.getId());
    }

    public void addCurrency(Currency currency) {
        jdbcTemplate.update("insert into currency (`name`, `rate`, `id`)" +
                        " values (?, ?, ?)",
                currency.getName(), currency.getRate(), currency.getId());
    }

    public void deleteSingleCurrency(Long currencyId) {
        jdbcTemplate.update("delete from currency where id =?", currencyId);
    }


}
