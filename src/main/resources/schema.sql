
-- Dumping structure for table currency_FX_database.currency
DROP TABLE IF EXISTS `currency`;

CREATE TABLE IF NOT EXISTS `currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` float DEFAULT NULL,
  PRIMARY KEY (`id`)
);

--exchange rates available here: https://www.xe.com/
INSERT INTO `currency` (`id`, `name`, `rate`) VALUES
	(1, 'USD', '1.08625'),
	(2, 'GBP', '0.87904'),
	(3, 'CAD', '1.53639'),
	(4, 'CHF', '1.05241'),
	(5, 'AUD', '1.71299'),
	(6, 'INR', '83.2161');
